from django.db import models


class Category(models.Model):
    title = models.CharField(max_length=64, verbose_name='Название', blank=True, null=True)

    def __str__(self):
        return self.title


class Article(models.Model):
    title = models.CharField(max_length=64, verbose_name='Название', blank=True, null=True)
    text = models.TextField(verbose_name='Описание обьявления', blank=True, null=True)
    owner = models.CharField(verbose_name='Автор', max_length=64, blank=True, null=True)
    price = models.PositiveSmallIntegerField(verbose_name='Цена')
    end_date = models.DateField(verbose_name='Дата окончания')
    category = models.ForeignKey(Category,on_delete=models.CASCADE, verbose_name='Категория', blank=True, null=True)


class ArticleImages(models.Model):
    article = models.ForeignKey(Article,on_delete=models.CASCADE, verbose_name='Обюявление', blank=True, null=True)
    image = models.ImageField(verbose_name='Изображение', upload_to='article_image/')


