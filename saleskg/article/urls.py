from django.urls import path,include
from.views import ArticleListView,ArticleDetailView


urlpatterns = [
    path('list/', ArticleListView.as_view(),name='article_list'),
    path('<int:pk>/', ArticleDetailView.as_view(),name='article_detail'),
]